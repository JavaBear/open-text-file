import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainFrame extends JFrame {

    private JPanel filesListPanel;
    private JPanel filePanel;
    private JPanel bottomPanel;
    private JMenuBar menuBar;
    private JDialog dialog;
    private String directory;
    private Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();

    public MainFrame() {
        setTitle("My Explorer");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        setBounds(dimension.width/2 - 350, dimension.height/2 - 250, 700, 500);

        menuBar = getMenu();
        setJMenuBar(menuBar);

//        filesListPanel = getFilesListPanel();
//        add(filesListPanel, BorderLayout.EAST);
//        filePanel = getFilePanel();
//
//        add(filePanel, BorderLayout.CENTER);
//        bottomPanel = new JPanel();
//        add(bottomPanel, BorderLayout.SOUTH);

        setVisible(true);
    }

    private JPanel getFilesListPanel() {
        JPanel filesListPanel = new JPanel();
        JTextArea textArea = new JTextArea();
        filesListPanel.add(textArea);
        return filesListPanel;
    }

    private JPanel getFilePanel() {
        JPanel filePanel = new JPanel();
        JTextArea textArea = new JTextArea();
        filePanel.add(textArea);
        return filePanel;
    }

    private JMenuBar getMenu() {
        JMenuBar menuBar = new JMenuBar();
        JMenu file = new JMenu("File");
        JMenuItem chooseDirectory = new JMenuItem("Choose directory");
        chooseDirectory.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dialog = createDialog("Choose directory", Dialog.ModalityType.APPLICATION_MODAL);
                dialog.setVisible(true);
            }
        });
        JMenuItem exit = new JMenuItem("Exit");

        file.add(chooseDirectory);
        file.addSeparator();
        file.add(exit);

        menuBar.add(file);
        return menuBar;
    }

    private JDialog createDialog(String title, Dialog.ModalityType modal) {
        JDialog dialog = new JDialog(this, title, modal);
        dialog.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        dialog.setBounds(dimension.width/2 - 200, dimension.height/2 - 50, 600, 100);

        JPanel dialogPanel = new JPanel();
        JTextField textField = new JTextField(50);
        JButton submit = new JButton("Submit");
        dialogPanel.add(textField);
        dialogPanel.add(submit);
        dialog.add(dialogPanel);

        submit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (textField.getText() != null) {
                    directory = textField.getText();
                } else {
                    JOptionPane.showMessageDialog(new JPanel(), "Directory must be filled", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        return dialog;
    }
}
